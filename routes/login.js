const express = require('express');
const { Get, Post } = require('../controllers/login');
const Router = express.Router();

Router.route('/login').get(Get).post(Post);

module.exports = Router;
