const express = require('express');
const app = express();
const loginRouter = require('./routes/login');
const port = 2001;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(express.static('controllers'));
app.set('view engine', 'ejs');

// erorr handling

// HOME
app.get('/', (req, res) => {
  res.render('index');
});

app.use(loginRouter);
// GAMES
app.get('/game', (req, res) => {
  res.render('pages/games/game.ejs');
});

app.use((req, res) => {
  res.status(404).render('pages/error/error.ejs');
  next();
});

app.listen(port, () => {
  console.log('server is running');
});
