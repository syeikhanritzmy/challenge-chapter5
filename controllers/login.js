const database = require('../database/users.json');
module.exports = {
  Get: (req, res) => {
    res.render('pages/Auth/Login.ejs');
  },
  Post: (req, res) => {
    let request = req.body;
    let userData = database;
    let found = false;
    userData.filter((account) => {
      if (
        request.email === account.email &&
        request.password === account.password
      ) {
        found = true;
      }
    });
    if (found) {
      return res.status(200).redirect('/game');
    } else {
      return res.status(401).send('cek again your email and password');
    }
  },
};
